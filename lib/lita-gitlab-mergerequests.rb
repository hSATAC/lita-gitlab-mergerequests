require "lita"
require 'gitlab'
require 'rufus-scheduler'

Lita.load_locales Dir[File.expand_path(
  File.join("..", "..", "locales", "*.yml"), __FILE__
)]

require "lita/handlers/gitlab_mergerequests"

Lita::Handlers::GitlabMergerequests.template_root File.expand_path(
  File.join("..", "..", "templates"),
 __FILE__
)
