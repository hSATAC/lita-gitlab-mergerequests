module Lita
  module Handlers
    class GitlabMergerequests < Handler
      # insert handler code here
      config :api_endpoint, type: String, required: true, default: ( ENV['GITLAB_API_ENDPOINT'].nil? ? "https://gitlab.com/api/v4" : ENV['GITLAB_API_ENDPOINT'] )
      config :personal_access_token, type: String, required: true, default: ENV['GITLAB_API_PRIVATE_TOKEN']

      PROJECTS_LIST_KEY = "gitlab_projects"
      REMINDER_SETTINGS_HASH_KEY = "gitlab_reminder"

      SCHEDULER = Rufus::Scheduler.new

      on :connected, :configure_gitlab
      on :connected, :set_reminder_cron

      route(/^gitlab\s+mr$/, :list_pending_mrs, command: true, restrict_to: [:gitlab], help: {
        "gitlab mr" => "List pending merge requests."
      })

      route(/^gitlab\s+mr\s+runas/, :runas, command: true, restrict_to: [:gitlab], help: {
        "gitlab runas" => "Query the current API operator account"
      })

      route(/^gitlab\s+mr\s+set\s+reminder\s+(.+)/, :set_reminder, command: true, restrict_to: [:gitlab], help: {
        "gitlab mr set reminder [CRON EXPRESSION] ('<min> <hour> <day> <month> <weekday> (<timezone>)')" => "Set cron expression for routinely deliver the pending merge requests list."
      })

      route(/^gitlab\s+mr\s+get\s+reminder/, :get_reminder, command: true, restrict_to: [:gitlab], help: {
        "gitlab mr get reminder" => "Get cron expression for routinely deliver the pending merge requests list."
      })

      route(/^gitlab\s+mr\s+remove\s+reminder/, :remove_reminder, command: true, restrict_to: [:gitlab], help: {
        "gitlab mr remove reminder" => "Remove cron expression for routinely deliver the pending merge requests list."
      })

      route(/^gitlab\s+mr\s+project\s+list/, :list_projects, command: true, restrict_to: [:gitlab], help: {
        "gitlab mr project list" => "List currently monitoring projects."
      })

      route(/^gitlab\s+mr\s+project\s+add\s+(.+)/, :add_project, command: true, restrict_to: [:gitlab], help: {
        "gitlab mr project add [PROJECT] (e.g: 'hiroshiyui/lita-gitlab-mergerequests')" => "Add a to be monitored project into list."
      })

      route(/^gitlab\s+mr\s+project\s+remove\s+(.+)/, :remove_project, command: true, restrict_to: [:gitlab], help: {
        "gitlab mr project remove [PROJECT]" => "Remove a currently monitored project from list."
      })

      def projects
        Gitlab.projects({membership: true}).auto_paginate
      end

      def projects_in_list
        redis.lrange(PROJECTS_LIST_KEY, 0, -1)
      end

      def pending_mrs
        pending_mrs = []
        projects_in_list.each do |p|
          merge_requests = Gitlab.merge_requests(p, {state: 'opened', wip: 'no'})
          if merge_requests.count > 0
            pending_mrs << {
              project: p,
              merge_requests: merge_requests
            }
          end
        end
        pending_mrs
      end

      def formatted_pending_mrs
        formatted_pending_mrs = []
        pending_mrs.each do |p_mr|
          formatted_pending_mrs << "In *'#{p_mr[:project]}'*"
          formatted_pending_mrs << p_mr[:merge_requests].map { |mr| "• #{mr.iid} <#{mr.web_url}|#{mr.title}> - #{(mr.assignee.nil? ? '_no assignee_' : mr.assignee.username)}" }.join("\n")
        end
        formatted_pending_mrs.join("\n")
      end

      def runas(response)
        api_user = Gitlab.client.user
        response.reply "Current API operator account is owned by: #{api_user.name}"
      end

      def list_pending_mrs(response)
        response.reply "List pending merge requests."
        if projects_in_list.empty?
          response.reply "Mmm... you haven't add any project yet."
        else
          response.reply formatted_pending_mrs
        end
        response.reply "Done."
      end

      def list_projects(response)
        response.reply "List currently monitoring projects."
        if projects_in_list.empty?
          response.reply "Mmm... you haven't add any project yet."
        else
          response.reply( projects_in_list.map{ |p| p.prepend("- ") }.join("\n") )
        end
      end

      def add_project(response)
        project = response.matches[0][0].strip
        response.reply "Add a to be monitored project into list."
        response.reply "Trying to add '#{project}'.."
        if projects.find { |p| p.path_with_namespace == project }
          response.reply "Found '#{project}'!"

          if redis.lrange(PROJECTS_LIST_KEY, 0, -1).include?(project)
            response.reply "Project is already in list."
          else
            response.reply "Adding project '#{project}' into list..."
            redis.rpush(PROJECTS_LIST_KEY, project)
            response.reply "Done! :tada:"
          end
        else
          response.reply "'#{project}' is not found in your project(s), you may want to check the input."
        end
      end

      def remove_project(response)
        project = response.matches[0][0].strip
        response.reply "Remove a currently monitored project from list."
        response.reply "Trying to remove '#{response.matches[0][0]}'.."
        if redis.lrange(PROJECTS_LIST_KEY, 0, -1).include?(project)
          response.reply "Project is in list, removing it..."
          redis.lrem(PROJECTS_LIST_KEY, 0, project)
          response.reply "Done! :tada:"
        else
          response.reply "#{project}' is not found in list."
        end
      end

      def configure_gitlab(_payload)
        Gitlab.configure do |c|
          c.endpoint = config.api_endpoint
          c.private_token = config.personal_access_token
        end
      end

      def set_reminder(response)
        cron_expression = response.matches[0][0].strip
        response.reply "Setting cron expression '#{cron_expression}' for you or the current channel."
        begin
          Rufus::Scheduler.parse(cron_expression)
          redis.hset(REMINDER_SETTINGS_HASH_KEY, "cron", cron_expression)
          redis.hset(REMINDER_SETTINGS_HASH_KEY, "user", response.message.source.user.id)
          redis.hset(REMINDER_SETTINGS_HASH_KEY, "room", response.message.source.room)
          set_reminder_cron
        rescue ArgumentError => error
          response.reply "Seems like you give me an invalid cron expression '#{cron_expression}' (*Error:* #{error.message})"
        rescue StandardError => error
          response.reply "Oops! (*Error:* #{error.message})"
        end
      end

      def reminder_settings
        reminder_settings = redis.hgetall(REMINDER_SETTINGS_HASH_KEY) # Hash
        if reminder_settings.empty?
          nil
        else
          reminder_settings
        end
      end

      def set_reminder_cron(_payload = nil)
        if reminder_settings
          log.info "Setting reminder as cron expression '#{reminder_settings["cron"]}'"
          SCHEDULER.cron_jobs.each(&:unschedule)
          SCHEDULER.cron reminder_settings["cron"] do |job|
            target = Source.new( user: reminder_settings["user"], room: reminder_settings["room"] )
            robot.send_messages( target, formatted_pending_mrs )
          end
        else
          log.info "No reminder found."
        end
      end

      def get_reminder(response)
        if reminder_settings
          response.reply "Current reminder cron expression is '#{reminder_settings["cron"]}'"
        else
          response.reply "No reminder found."
        end
      end

      def remove_reminder(response)
        if reminder_settings
          response.reply "Removing reminder..."
          redis.del(REMINDER_SETTINGS_HASH_KEY)
          SCHEDULER.cron_jobs.each(&:unschedule)
          response.reply "Done!"
        else
          response.reply "No reminder found."
        end
      end

      Lita.register_handler(self)
    end
  end
end
