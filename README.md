# lita-gitlab-mergerequests

A Lita handler to help you keep track of your merge requests.

This plugin is heavily inspired by [lita-pullrequests](https://github.com/taylorlapeyre/lita-pullrequests/).

## Installation

Add lita-gitlab-mergerequests to your Lita instance's Gemfile:

``` ruby
gem "lita-gitlab-mergerequests"
```

## Configuration

In your `lita_config.rb`:

* `config.handlers.gitlab_mergerequests.api_endpoint`: If you're using a different gitlab API endpoint, please set this configuration, or override it by setting an environment variable `GITLAB_API_ENDPOINT`. (default: `https://gitlab.com/api/v4`)
* `config.handlers.gitlab_mergerequests.personal_access_token`: Your API personal access token. Get one from [here](https://gitlab.com/profile/personal_access_tokens). You may override it by setting an environment variable `GITLAB_API_PRIVATE_TOKEN`.

## Usage

Grant permission to others by adding them into `gitlab` authorization group.

`Lita: auth add [ID] gitlab`

The members of `gitlab` authorization group are able to use the following commands:

* Lita: gitlab mr - List pending merge requests.
* Lita: gitlab runas - Query the current API operator account
* Lita: gitlab mr set reminder [CRON EXPRESSION] (`<min> <hour> <day> <month> <weekday> (<timezone>)`) - Set cron expression for routinely deliver the pending merge requests list.
* Lita: gitlab mr get reminder - Get cron expression for routinely deliver the pending merge requests list.
* Lita: gitlab mr remove reminder - Remove cron expression for routinely deliver the pending merge requests list.
* Lita: gitlab mr project list - List currently monitoring projects.
* Lita: gitlab mr project add [PROJECT] (e.g: 'hiroshiyui/lita-gitlab-mergerequests') - Add a to be monitored project into list.
* Lita: gitlab mr project remove [PROJECT] - Remove a currently monitored project from list.