Gem::Specification.new do |spec|
  spec.name          = "lita-gitlab-mergerequests"
  spec.version       = "0.1.0"
  spec.authors       = ["Hui-Hong You"]
  spec.email         = ["hiroshi@ghostsinthelab.org"]
  spec.description   = "A Lita plugin that help to manage GitLab merge requests, includes pending MRs reporting, assignee management, etc."
  spec.summary       = "Help to manage GitLab merge requests"
  spec.homepage      = "https://gitlab.com/hiroshiyui/lita-gitlab-mergerequests"
  spec.license       = "GPLv3"
  spec.metadata      = { "lita_plugin_type" => "handler" }

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency "lita", ">= 4.7"
  spec.add_runtime_dependency 'gitlab', '~> 4.7'
  spec.add_runtime_dependency 'rufus-scheduler', '~> 3.5', '>= 3.5.2'

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "pry-byebug"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "rack-test"
  spec.add_development_dependency "rspec", ">= 3.0.0"
  spec.add_development_dependency 'ruby-debug-ide', '~> 0.7.0.beta7'
end